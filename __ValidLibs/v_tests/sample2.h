Foo.h:

/** @file sample2.h*/
/**
 * @addtogroup TEST_GROUP
 *
 * @{
 */

/** @brief My Class. */     
class Foo {
    public:
        void method();
};

/** @} */

